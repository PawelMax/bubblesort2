package pl.SDA;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class App {
    public static void main(String[] args) {

        BubbleSort bubbleSort = new BubbleSort();

        byte listSize;
        Scanner sc = new Scanner(System.in);

        System.out.println("Podaj ilość elementów listy");
        listSize = sc.nextByte();

        //lista z liczbami
        List<Integer> list = new ArrayList<Integer>(listSize);

        for (int i : list) {

            System.out.println("podaj liczbę");

            // wiem, że listy nie przyjmują typów primitywnych, dlatego jest Integer, ale jak go wprowadzić z klawiatury?
//            list.add(i) = sc.nextInt();
        }

        BubbleSort.getSortedIntList(list);


    }
}
