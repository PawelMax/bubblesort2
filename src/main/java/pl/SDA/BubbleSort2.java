package pl.SDA;

public class BubbleSort2 {

    //metoda sortowania bąbelkowego
    public static void getSortedIntList (int list[]) {

        int temp;

        for (int i = 0; i < list.length - 1; i++) {
            for (int j = 0; j < list.length - 1; j++) {
                if (list[j] > list[j+1]) {
                   temp = list[j+1];
                   list[j+1] = list[j];
                   list[j] = temp;
                }
            }
        }

        for (int i = 0; i < list.length; i++) {

            System.out.print(list[i] + "|");

        }

    }
}
